//
//  UrlFile.swift
//  3C
//
//  Created by Namespace  on 08/06/18.
//  Copyright © 2018 Namespace . All rights reserved.
//

import Foundation

enum Urls:String{
    case checkAuthentication = "http://attendance.hipster-inc.com/api/check-authentication"
    case checkQR = "http://attendance.hipster-inc.com/api/check-qr-valid"
    case checkIfOverWrite = "http://attendance.hipster-inc.com/api/check-if-overwrite"
    case setAttendance = "http://attendance.hipster-inc.com/api/set-attendance"
    
}

extension FileManager {
    class func documentsDir() -> String {
        var paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true) as [String]
        return paths[0]
    }
    
    class func cachesDir() -> String {
        var paths = NSSearchPathForDirectoriesInDomains(.cachesDirectory, .userDomainMask, true) as [String]
        return paths[0]
    }
}
