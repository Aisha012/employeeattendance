//
//  UIDeviceExtension.swift
//  Employee_Attendance_App
//
//  Created by Renendra Saxena on 05/11/18.
//  Copyright © 2018 Developers Group. All rights reserved.
//

import Foundation
import  UIKit

extension UIDevice {
    
        var iPhoneX: Bool {
            return UIScreen.main.nativeBounds.height == 2436
        }
        var iPhone: Bool {
            return UIDevice.current.userInterfaceIdiom == .phone
        }
        enum ScreenType: String {
            case iPhones_4_4S = "iPhone 4 or iPhone 4S"
            case iPhones_5_5s_5c_SE = "iPhone 5, iPhone 5s, iPhone 5c or iPhone SE"
            case iPhones_6_6s_7_8 = "iPhone 6, iPhone 6S, iPhone 7 or iPhone 8"
            case iPhones_6Plus_6sPlus_7Plus_8Plus = "iPhone 6 Plus, iPhone 6S Plus, iPhone 7 Plus or iPhone 8 Plus"
            case iPhones_X_XS = "iPhone X or iPhone XS"
            case iPhone_XR = "iPhone XR"
            case iPhone_XSMax = "iPhone XS Max"
            case unknown
        }
        var screenType: ScreenType {
            switch UIScreen.main.nativeBounds.height {
            case 960:
                return .iPhones_4_4S
            case 1136:
                return .iPhones_5_5s_5c_SE
            case 1334:
                return .iPhones_6_6s_7_8
            case 1792:
                return .iPhone_XR
            case 1920, 2208:
                return .iPhones_6Plus_6sPlus_7Plus_8Plus
            case 2436:
                return .iPhones_X_XS
            case 2688:
                return .iPhone_XSMax
            default:
                return .unknown
            }
        }
//    func DeviceName()-> String {
//
//        var myDeviceName : String = String()
//        var modelName: String {
//            var systemInfo = utsname()
//            uname(&systemInfo)
//            let machineMirror = Mirror(reflecting: systemInfo.machine)
//            let identifier = machineMirror.children.reduce("") { identifier, element in
//                guard let value = element.value as? Int8, value != 0 else { return identifier }
//                return identifier + String(UnicodeScalar(UInt8(value)))
//            }
//            return identifier
//        }
//
//        let deviceNamesByCode : [String: String] = ["iPod1,1":"iPod Touch 1",
//                                                    "iPod2,1":"iPod Touch 2",
//                                                    "iPod3,1":"iPod Touch 3",
//                                                    "iPod4,1":"iPod Touch 4",
//                                                    "iPod5,1":"iPod Touch 5",
//                                                    "iPod7,1":"iPod Touch 6",
//                                                    "iPhone1,1":"iPhone",
//                                                    "iPhone1,2":"iPhone ",
//                                                    "iPhone2,1":"iPhone ",
//                                                    "iPhone3,1":"iPhone 4",
//                                                    "iPhone3,2":"iPhone 4",
//                                                    "iPhone3,3":"iPhone 4",
//                                                    "iPhone4,1":"iPhone 4s",
//                                                    "iPhone5,1":"iPhone 5",
//                                                    "iPhone5,2":"iPhone 5",
//                                                    "iPhone5,3":"iPhone 5c",
//                                                    "iPhone5,4":"iPhone 5c",
//                                                    "iPhone6,1":"iPhone 5s",
//                                                    "iPhone6,2":"iPhone 5s",
//                                                    "iPhone7,2":"iPhone 6",
//                                                    "iPhone7,1":"iPhone 6 Plus",
//                                                    "iPhone8,1":"iPhone 6s",
//                                                    "iPhone8,2":"iPhone 6s Plus",
//                                                    "iPhone8,4":"iPhone SE",
//                                                    "iPad2,1":"iPad 2",
//                                                    "iPad2,2":"iPad 2",
//                                                    "iPad2,3":"iPad 2",
//                                                    "iPad2,4":"iPad 2",
//                                                    "iPad3,1":"iPad 3",
//                                                    "iPad3,2":"iPad 3",
//                                                    "iPad3,3":"iPad 3",
//                                                    "iPad3,4":"iPad 4",
//                                                    "iPad3,5":"iPad 4",
//                                                    "iPad3,6":"iPad 4",
//                                                    "iPad4,1":"iPad Air",
//                                                    "iPad4,2":"iPad Air",
//                                                    "iPad4,3":"iPad Air",
//                                                    "iPad5,3":"iPad Air 2",
//                                                    "iPad5,4":"iPad Air 2",
//                                                    "iPad2,5":"iPad Mini",
//                                                    "iPad2,6":"iPad Mini",
//                                                    "iPad2,7":"iPad Mini",
//                                                    "iPad4,4":"iPad Mini 2",
//                                                    "iPad4,5":"iPad Mini 2",
//                                                    "iPad4,6":"iPad Mini 2",
//                                                    "iPad4,7":"iPad Mini 3",
//                                                    "iPad4,8":"iPad Mini 3",
//                                                    "iPad4,9":"iPad Mini 3",
//                                                    "iPad5,1":"iPad Mini 4",
//                                                    "iPad5,2":"iPad Mini 4",
//                                                    "iPad6,3":"iPad Pro",
//                                                    "iPad6,4":"iPad Pro",
//                                                    "iPad6,7":"iPad Pro",
//                                                    "iPad6,8":"iPad Pro",
//                                                    "AppleTV5,3":"Apple TV",
//                                                    "i386":"Simulator",
//                                                    "x86_64":"Simulator"
//
//        ]
//
//        if model.characters.count > 0 {
//            myDeviceName = deviceNamesByCode[modelName]!
//        }else{
//            myDeviceName = UIDevice.current.model
//        }
//
//        print("myDeviceName==\(myDeviceName)")
//        return myDeviceName
//
//    }
    
}
