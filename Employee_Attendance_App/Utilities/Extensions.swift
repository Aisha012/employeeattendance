//
//  Extensions.swift
//  3C
//
//  Created by Namespace  on 07/06/18.
//  Copyright © 2018 Namespace . All rights reserved.

import Foundation
import SHSnackBarView
import UIKit
import MBProgressHUD


var appDelegate = UIApplication.shared.delegate as! AppDelegate
var filterData = NSMutableDictionary()
var customLoader = UIImageView()


extension UIViewController {
    
    enum intervals:Int {
        case short = 1
        case long  = 3
    }
    var device: UIDevice
    {   get{
        return UIDevice()
        }
    }
    

    var jsonDecoder:JSONDecoder{
        get{
            return JSONDecoder()
        }
    }
    
    var jsonEncoder:JSONEncoder{
        get{
            return JSONEncoder()
        }
    }

    var snackbarView:snackBar{
        get{
            return snackBar()
        }
    }

//
    func showSnackBarWithMessage(message:String, color:UIColor, txtColor:UIColor? = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0),interval:intervals){
        snackbarView.showSnackBar(view: self.view, bgColor: color, text: message, textColor: txtColor!, interval: interval.rawValue)
    }

//    var  manager:AFHTTPSessionManager{
//        get{
//            return AFHTTPSessionManager()
//        }
//    }

    func showHud()
    {
        let window = UIApplication.shared.delegate?.window
        let hud = MBProgressHUD.showAdded(to: window as! UIView, animated: true)
        hud.label.text = "Please Wait..."
    }

    func closeHud()
    {
//        APESuperHUD.dismissAll(animated: true)
        let window = UIApplication.shared.delegate?.window
        MBProgressHUD.hideAllHUDs(for: window as! UIView, animated: true)

    }
        func showHudWithDelay() {
//            let mainStoryboard: UIStoryboard = UIStoryboard(name: "TabController", bundle: nil)
//            let internetVC = mainStoryboard.instantiateViewController(withIdentifier: "InternetConnectionController")
//            internetVC.view.frame = self.view.bounds;
//
//            self.view.addSubview(internetVC.view)
        }

    // MARK:- Connectivity Check
    func isConnectivity() -> Bool{
        let reachability = Reachability()
        if (reachability?.isReachable)!{
            return true
        }else{
            return false
        }
    }
    func changeIntoURL<T : Codable>(requestData : T , url : String) -> URL {
        let jsonData = try! jsonEncoder.encode(requestData)
        let json  = try! JSONSerialization.jsonObject(with: jsonData, options: JSONSerialization.ReadingOptions.allowFragments)
        let dict = (json as! NSDictionary)
        var st : String = ""
        for (dta , value) in dict{
            st = st + (dta as! String) + "=\(value)&"

        }
        st = String((url + st).dropLast())

        print(st)
        let apiURL = URL(string: st)
        print(apiURL)
        return apiURL!
    }

    func postDataFromNet<T:Codable>(url:String,requestData:T, selector:Selector ,requestMethod : String = "POST" ){

        //        showLoadingHUD(to_view: self.view)
        if isConnectivity(){
        showHud()

        let jsonData = try! jsonEncoder.encode(requestData)
            let json  = try! JSONSerialization.jsonObject(with: jsonData, options: JSONSerialization.ReadingOptions.allowFragments)
            print(json)
            print(jsonData)

            var urlRequest = URLRequest(url: URL(string: url)!)
            urlRequest.addValue("application/json", forHTTPHeaderField: "Content-Type")
            urlRequest.httpMethod = requestMethod
            urlRequest.httpBody = jsonData
           let task  =  URLSession.shared.dataTask(with: urlRequest) { (data, response, error) in
                if let err = error{
                      DispatchQueue.main.async {
                    self.closeHud()
                    }
                    print(err)
                    return
                }
                if let httpResponse = response as? HTTPURLResponse{
                    switch (httpResponse.statusCode) {
                    case 200 , 400 :
                        do{
//                            let datatopass  = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.allowFragments) as! [String :Any]
//                            let datatopass  = try JSONSerialization.data(withJSONObject: data!, options: JSONSerialization.WritingOptions.prettyPrinted)
                            DispatchQueue.main.async {
                                self.closeHud()
                                self.perform(selector, with: data!)
                            }

                        }catch{

                        }
                        break
                    case 404:
                        do{
                            let datatopass  = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.allowFragments) as! [String :Any]
                              DispatchQueue.main.async {
                            self.closeHud()
                            self.showSnackBarWithMessage(message: datatopass["message"]! as! String, color: #colorLiteral(red: 0.7450980544, green: 0.1568627506, blue: 0.07450980693, alpha: 1), interval: .long)
                            print(datatopass)
                            }
                        }catch{

                        }

                    case 401:
                        break;
                    default:
                        break;

                }
            }
        }
      task.resume()

        }else{
            self.showHudWithDelay()
        }

    }
    func postDataFromNetLogin<T:Codable>(url:String,requestData:T, selector:Selector ,requestMethod : String = "POST" ){

        //        showLoadingHUD(to_view: self.view)
        if isConnectivity(){
            showHud()

            let jsonData = try! jsonEncoder.encode(requestData)
            let json  = try! JSONSerialization.jsonObject(with: jsonData, options: JSONSerialization.ReadingOptions.allowFragments)
            print(json)
            print(jsonData)

            var urlRequest = URLRequest(url: URL(string: url)!)
            urlRequest.addValue("text/html", forHTTPHeaderField: "Content-Type")
            urlRequest.httpMethod = requestMethod
            urlRequest.httpBody = jsonData
            let task  =  URLSession.shared.dataTask(with: urlRequest) { (data, response, error) in
                if let err = error{
                    DispatchQueue.main.async {
                        self.closeHud()
                    }
                    print(err)
                    return
                }
                if let httpResponse = response as? HTTPURLResponse{
                    switch (httpResponse.statusCode) {
                    case 200 :
                        do{
                            //                            let datatopass  = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.allowFragments) as! [String :Any]
                            //                            let datatopass  = try JSONSerialization.data(withJSONObject: data!, options: JSONSerialization.WritingOptions.prettyPrinted)
//                            let data = try JSONSerialization.jsonObject(with: data!, options: [])
                            let data = try JSONSerialization.data(withJSONObject: data!, options: [])

                            DispatchQueue.main.async {
                                self.closeHud()
                                self.perform(selector, with: data)
                            }

                        }catch{

                        }
                        break
                    case 404:
                        do{
                            let datatopass  = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.allowFragments) as! [String :Any]
                            DispatchQueue.main.async {
                                self.closeHud()
                                self.showSnackBarWithMessage(message: datatopass["message"]! as! String, color: #colorLiteral(red: 0.7450980544, green: 0.1568627506, blue: 0.07450980693, alpha: 1), interval: .long)
                                print(datatopass)
                            }
                        }catch{

                        }

                    case 401:
                        break;
                    default:
                        break;

                    }
                }
            }
            task.resume()

        }else{
            self.showHudWithDelay()
        }



        //        manager.requestSerializer.setValue("x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        //        manager.post(url, parameters: json, progress: nil, success: {[weak self] (task, result) in
        //            do {
        //                let datatopass  = try JSONSerialization.data(withJSONObject: result!, options: JSONSerialization.WritingOptions.prettyPrinted)
        //                    self?.perform(selector, with: datatopass)
        //
        //            }catch{
        //
        //            }
        //            print(result)
        //            self?.closeHud()
        //            //            self?.hideLoadingHUD(for_view: self!.view)
        //        }) { (task, error) in
        //
        //
        //
        //            let response  = task?.response as! HTTPURLResponse
        //            print(response.statusCode)
        //
        //
        //            }
        //            //            self.showSnackBarWithMessage(message:"Some thing went wrong.", color: #colorLiteral(red: 0.9254902005, green: 0.2352941185, blue: 0.1019607857, alpha: 1), interval: .long)
        //            self.closeHud()
        //
        //            print(error)
        //            return
        //
        //        }



    }



    func getDataFromNetIPAddress(url:String, selector:Selector){

        if isConnectivity(){
            var characterSet = CharacterSet.urlQueryAllowed
            characterSet.remove(charactersIn: " ")
            let str = url.addingPercentEncoding(withAllowedCharacters: characterSet)

            var urlRequest = URLRequest(url: URL(string: str!)!)
            urlRequest.addValue("x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
            urlRequest.httpMethod = "Get"
            let task  =  URLSession.shared.dataTask(with: urlRequest) { (data, response, error) in
                if let err = error{
                    print(err)
                    return
                }
                if let httpResponse = response as? HTTPURLResponse{
                    switch (httpResponse.statusCode) {
                    case 200 :
                        do{
                            DispatchQueue.main.async {
                                self.perform(selector, with: data!)
                            }

                        }catch{

                        }
                        break
                    case 404:
                        do{
                            let datatopass  = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.allowFragments) as! [String :Any]
                            DispatchQueue.main.async {
                                self.showSnackBarWithMessage(message: datatopass["message"]! as! String, color: #colorLiteral(red: 0.5725490451, green: 0, blue: 0.2313725501, alpha: 1), interval: .long)
                                print(datatopass)
                            }
                        }catch{

                        }

                    case 401:
                        break;
                    default:
                        break;

                    }
                }
            }
            task.resume()

        }else{
        }

    }



        func getDataFromNet(url:String, selector:Selector){

            if isConnectivity(){
                showHud()
                var characterSet = CharacterSet.urlQueryAllowed
                characterSet.remove(charactersIn: " ")
//                characterSet.insert(charactersIn: "?& ")
                let str = url.addingPercentEncoding(withAllowedCharacters: characterSet)

                var urlRequest = URLRequest(url: URL(string: str!)!)
                urlRequest.addValue("x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
                urlRequest.httpMethod = "Get"
                let task  =  URLSession.shared.dataTask(with: urlRequest) { (data, response, error) in
                    if let err = error{
                        print(err)
                        return
                    }
                    if let httpResponse = response as? HTTPURLResponse{
                        switch (httpResponse.statusCode) {
                        case 200 :
                            do{
                                DispatchQueue.main.async {
                                    self.closeHud()
                                    self.perform(selector, with: data!)
                                }

                            }catch{

                            }
                            break
                        case 404:
                            do{
                                let datatopass  = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.allowFragments) as! [String :Any]
                                DispatchQueue.main.async {
                                self.closeHud()
                                self.showSnackBarWithMessage(message: datatopass["message"]! as! String, color: #colorLiteral(red: 0.5725490451, green: 0, blue: 0.2313725501, alpha: 1), interval: .long)
                                print(datatopass)
                                }
                            }catch{

                            }

                        case 401:
                            break;
                        default:
                            break;

                        }
                    }
                }
                task.resume()

            }else{
                self.showHudWithDelay()
            }

        }


//    func postDataFromNetWithImage<T:Codable>(url:String, requestData:T, image : Data, selector:Selector){
//        let jsonData = try! jsonEncoder.encode(requestData)
//        let json  = try! JSONSerialization.jsonObject(with: jsonData, options: JSONSerialization.ReadingOptions.allowFragments)
//        //        showLoadingHUD(to_view: self.view)
//        showHud()
//
//        manager.post(url, parameters: json, constructingBodyWith: { (imgdata) in
//            do{
//
//                _ = try imgdata.appendPart(withFileData: image, name: "image.png", fileName:  "\(Date().timeIntervalSince1970).png", mimeType:"image/png")
//                //appendPart(withForm: image , name: "image.png" )
//
//            }catch{
//            }
//        }, progress: nil, success: { (task, result) in
//            do {
//                let datatopass  = try JSONSerialization.data(withJSONObject: result!, options: JSONSerialization.WritingOptions.prettyPrinted)
//                self.perform(selector, with: datatopass)
//
//            }catch{
//
//            }
//            //            self.hideLoadingHUD(for_view: self.view)
//            self.closeHud()
//            print(result)
//        }) { (task, err) in
//            print(err)
//        }
//
//    }


//    func deleteDataFromNet(url:String, selector:Selector){
//        manager.delete(url, parameters: nil, success: {[weak self](task, result) in
//            do {
//                let datatopass  = try JSONSerialization.data(withJSONObject: result!, options: JSONSerialization.WritingOptions.prettyPrinted)
//                self?.perform(selector, with: datatopass)
//            }catch{
//
//            }        }) { (task, error) in
//                print(error)
//        }
//    }

}

extension String {
    func hexStringToUIColor () -> UIColor {
        var cString:String = self.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
}
extension String {
    func localize(bundle: Bundle = .main, tableName: String = "Localizable") -> String {
        
        let path = Bundle.main.path(forResource: "fr", ofType: "lproj")
        let bundle = Bundle.init(path: path!)
        return NSLocalizedString(self, tableName: tableName, bundle: bundle!,value: "**\(self)**", comment: "")
    }

}
