//
//  UIExtensions.swift
//  CoopCommerce
//
//  Created by Beeline2 on 10/25/16.
//  Copyright © 2016 Brevity. All rights reserved.
//

import UIKit

@IBDesignable extension UIView {
    @IBInspectable var borderColor:UIColor? {
        set {
            layer.borderColor = newValue!.cgColor
        }
        get {
            if let color = layer.borderColor {
                return UIColor(cgColor:color)
            }
            else {
                return nil
            }
        }
    }
    @IBInspectable var borderWidth:CGFloat {
        set {
            layer.borderWidth = newValue
        }
        get {
            return layer.borderWidth
        }
    }
    @IBInspectable var cornerRadius:CGFloat {
        set {
            layer.cornerRadius = newValue
            clipsToBounds = newValue > 0
        }
        get {
            return layer.cornerRadius
        }
    }
}

@IBDesignable extension UITextView {
    @IBInspectable var placeHolderText:NSString? {
        set {
            self.text = newValue as! String
        }
        get {
            if let text = self.text {
                return text as NSString?
            }
            else {
                return nil
            }
        }
    }
    @IBInspectable var placeHolderColor:UIColor {
        set {
            self.textColor = newValue
        }
        get {
            if let color = self.textColor {
                return color
            } else {
                return UIColor.lightGray
            }
        }
    }
}


extension String{
    func localized()->String{
        return NSLocalizedString(self, comment: "")
    }
    
    fileprivate func retrieveLocalized(){
        let path = Bundle.main.path(forResource: "Localizable", ofType: "strings")
        let localizedDict =  NSMutableDictionary(contentsOfFile: path!)
        guard let allKeys = localizedDict?.allKeys else {
            return
        }
        
    }
}


extension UIColor {
    convenience init(red: Int, green: Int, blue: Int) {
        assert(red >= 0 && red <= 255, "Invalid red component")
        assert(green >= 0 && green <= 255, "Invalid green component")
        assert(blue >= 0 && blue <= 255, "Invalid blue component")
        
        self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
    }
    
    convenience init(rgb: Int) {
        self.init(
            red: (rgb >> 16) & 0xFF,
            green: (rgb >> 8) & 0xFF,
            blue: rgb & 0xFF
        )
    }
}

