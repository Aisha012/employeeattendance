//
//  LoginViewController.swift
//  Employee_Attendance_App
//
//  Created by Renendra Saxena on 26/10/18.
//  Copyright © 2018 Developers Group. All rights reserved.
//

import UIKit
import SystemConfiguration

class LoginViewController: UIViewController {
    
    @IBOutlet var mobileTxtField : UITextField!
    @IBOutlet var checkInBtn : UIButton!
    @IBOutlet var checkOutBtn : UIButton!

    //    MARK: Initial Methods
    
    override func viewDidLoad() {
        UserDefaults.standard.set("checkin", forKey: "type")
        super.viewDidLoad()
        checkInBtn.isSelected = true
        checkOutBtn.isSelected = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    var mobile_num : String?
    var msg : String?
    func validateFields()->Bool{
//        c6f3654e0d8ff89a android device id
        if !mobileTxtField.isTextFieldEmpty(){
            msg = "Please Enter Mobile Number."
            self.performSegue(withIdentifier: "popUpOverSegue", sender: "notValidContact")

//            self.showSnackBarWithMessage(message:"Please Enter Mobile Number.", color:#colorLiteral(red: 0.7450980544, green: 0.1568627506, blue: 0.07450980693, alpha: 1),interval:intervals.long)
            return false
        }
//        if !mobileTxtField.isPhoneNumber{
//            msg = "Enter a valid Mobile Number."
//            self.performSegue(withIdentifier: "popUpOverSegue", sender: "notValidContact")
//
////            self.showSnackBarWithMessage(message:"Enter a valid Mobile Number.", color:#colorLiteral(red: 0.7450980544, green: 0.1568627506, blue: 0.07450980693, alpha: 1),interval:intervals.long)
//            return false
//
//        }
        return true
    }
    
    //    MARK: Buttons Action
    
    @IBAction func checkInBtnTapped(_ sender : UIButton){
        self.view.endEditing(true)
        checkInBtn.isSelected = !checkInBtn.isSelected
        checkOutBtn.isSelected = !checkOutBtn.isSelected

//        if validateFields(){
            UserDefaults.standard.set("checkin", forKey: "type")
            UserDefaults.standard.synchronize()
//        }
       
    }
    @IBAction func checkOutBtnTapped(_ sender : UIButton){
        self.view.endEditing(true)
        checkOutBtn.isSelected = !checkOutBtn.isSelected
        checkInBtn.isSelected = !checkInBtn.isSelected
//        if validateFields(){
            UserDefaults.standard.set("checkout", forKey: "type")
            UserDefaults.standard.synchronize()
//        }
    }
    @IBAction func submitBtnTapped(_ sender : UIButton){
        self.view.endEditing(true)
        if validateFields(){
            mobile_num = mobileTxtField.text
            UserDefaults.standard.set(mobile_num, forKey: "mobile_num")
            UserDefaults.standard.synchronize()
//            let request = LoginRequest(mobile_number: "7388877707", device_id: "85759ff87e05efc7")
            
                    let request = LoginRequest(mobile_number: mobile_num, device_id: UserDefaults.standard.value(forKey: "device_id") as! String, model_name:
                        UIDevice.current.screenType.rawValue, user_name: UIDevice.current.name, version_code: UIDevice.current.systemVersion, model:UIDevice.current.model)

            self.postDataFromNet(url: Urls.checkAuthentication.rawValue , requestData: request, selector: #selector(self.handleNetResponse(data:)))
        }
        
    }
    @objc func handleNetResponse(data:Data?) {
        if let dat = data{
            do{
                let mappedValue = try jsonDecoder.decode(LoginResponse.self, from: dat)
                if let res = mappedValue.data{
                    print(mappedValue.data)
                    UserDefaults.standard.set(mappedValue.data?.id, forKey: "user_id")
                    UserDefaults.standard.synchronize()
                    self.performSegue(withIdentifier: "QRScanVC", sender: nil)
                }else{
                    msg = mappedValue.message!
                    SweetAlert().showAlert(msg!, subTitle: "Please try again later!", style:.error, buttonTitle: "OK") { (Void) in
                        return
                        
                    }

//                    msg = "Contact your manager & get the device registered."
//                    self.performSegue(withIdentifier: "popUpOverSegue", sender: "notRegisterMobile")
                    return
                }
            }catch let err {
                print(err)
            }
        }
    }
    
    
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let identifer = segue.identifier
        switch  identifer {
        case "popUpOverSegue":
            let dest = segue.destination as! PopUpVC
            dest.fromVC = sender as! String
            dest.message = msg
            return
        case "QRScanVC" :
            let dest = segue.destination  as! QRScanVC
            dest.mobile_number = mobile_num!
            break
        default:
            break
        }
     }
}
