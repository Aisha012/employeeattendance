//
//  FetchMobileNumberVC.swift
//  Employee_Attendance_App
//
//  Created by Renendra Saxena on 29/10/18.
//  Copyright © 2018 Developers Group. All rights reserved.
//

import UIKit

class FetchMobileNumberVC: UIViewController , UITableViewDelegate,UITableViewDataSource {
   

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
//    MARK: table view delegate and data source methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    var cell : fetchContactCell!
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0{
            cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! fetchContactCell
        }else{
            cell = tableView.dequeueReusableCell(withIdentifier: "NoneCell", for: indexPath) as! fetchContactCell
            
        }
        
        return cell
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
class fetchContactCell : UITableViewCell{
    @IBOutlet var mobileNumber : UILabel!
}
