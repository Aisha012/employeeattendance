//
//  QRScanVC.swift
//  Employee_Attendance_App
//
//  Created by Renendra Saxena on 29/10/18.
//  Copyright © 2018 Developers Group. All rights reserved.
//

import UIKit
//import QRCodeReader
import AVFoundation


//@available(iOS 10.2, *)
class QRScanVC: UIViewController , AVCaptureMetadataOutputObjectsDelegate //, QRCodeReaderViewControllerDelegate {
{
    @IBOutlet weak var square: UIView!
    @IBOutlet weak var frontView: UIView!
    @IBOutlet weak var frontTitle: UILabel!

    var current_time : String?
    var access_point_id : String?
    var qrcode_time  : String?
    let type =  UserDefaults.standard.value(forKey: "type") as! String
    var video = AVCaptureVideoPreviewLayer()
    let session = AVCaptureSession()
    var mobile_number : String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.navigationBar.barTintColor = UIColor.black
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor : UIColor.white]
        let button = UIButton(type: UIButtonType.custom)
        button.setImage(UIImage(named: "logout"), for: .normal)
        button.addTarget(self, action:#selector(logoutBtnTapped(_:)), for: .touchUpInside)
        button.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
        let barButton = UIBarButtonItem(customView: button)
        self.navigationItem.rightBarButtonItem = barButton
        self.navigationItem.setHidesBackButton(true, animated:true)
        self.navigationItem.title = "Scan QR"
        //Creating session
        let captureDevice = AVCaptureDevice.default(for: AVMediaType.video)
        if captureDevice != nil{
            do
            {
                let input = try AVCaptureDeviceInput(device: (captureDevice)!)
                session.addInput(input)
            }
            catch
            {
                print ("ERROR")
            }
            
            let output = AVCaptureMetadataOutput()
            session.addOutput(output)
            output.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
            output.metadataObjectTypes = [AVMetadataObject.ObjectType.qr]
            video = AVCaptureVideoPreviewLayer(session: session)
            //        let points = CGPoint(x: 0, y: view.frame.size.width/2)
            //        let sizes = CGSize(width: view.layer.bounds.width-20, height: view.frame.size.width)
            video.frame = view.layer.bounds
            //            CGRect(origin: points, size: sizes)//
            view.layer.addSublayer(video)
            self.view.bringSubview(toFront: square)
            self.view.bringSubview(toFront: frontTitle)
            session.startRunning()
            
        }else{
            return
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
        
    }
    
   

    @objc func logoutBtnTapped(_ sender : UIButton){
    print("logout")
        self.navigationController?.popViewController(animated: true)
    }
    func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {
        
        if metadataObjects != nil && metadataObjects.count != 0
        {
            if let object = metadataObjects[0] as? AVMetadataMachineReadableCodeObject
            {
                if object.type == AVMetadataObject.ObjectType.qr
                {
                    self.session.stopRunning()
                    
                    //                    let alert = UIAlertController(title: "QR Code", message: object.stringValue, preferredStyle: .alert)
                    //                    alert.addAction(UIAlertAction(title: "Retake", style: .default, handler: {
                    //                        (nil) in
                    //                        self.session.startRunning()
                    //
                    //                    }))
                    //                    alert.addAction(UIAlertAction(title: "Confirm", style: .default, handler: { (nil) in
                    //                                APi call
                    
                    //                                Get the access point id from QR Code
                    //                    "Current Time >2018-11-16 13:26:31>30"
                    if object.stringValue != nil  && object.stringValue?.components(separatedBy: ">")[0] == "Current Time "{
                        self.access_point_id = object.stringValue?.components(separatedBy: ">")[2]
                        self.qrcode_time = object.stringValue?.components(separatedBy: ">")[1]
                        //
                        
                        //                        self.access_point_id = object.stringValue?.components(separatedBy: ">")[0] != nil  && object.stringValue?.components(separatedBy: ">")[0] == "Current Time" ?object.stringValue?.components(separatedBy: ">")[2]: ""
                        //                        self.qrcode_time = object.stringValue?.components(separatedBy: ">") != nil && object.stringValue?.components(separatedBy: ">")[0] == "Current Time" ?object.stringValue?.components(separatedBy: ">")[1]: ""
                        //
                        
                        //                                Get the current time form QRcode
                        let currentDateTime = Date()
                        //                                Change current time format
                        let format = DateFormatter()
                        format.dateFormat =  "yyyy-MM-dd HH:mm:ss"
                        self.current_time = format.string(from: currentDateTime)
                        //                        Check QR Is Valid or not
                        let request = QRRequest(current_time: self.current_time, qrcode_time: self.qrcode_time ,mobile_number : mobile_number)
                        self.postDataFromNet(url: Urls.checkQR.rawValue , requestData: request, selector: #selector(self.handleNetResponse(data:)))
                        
                        //                    }))
                        
                        //                    present(alert, animated: true, completion: nil)
                    }else{
                        self.navigationController?.popViewController(animated: true)
                        
                    }
                }
            }
        }
    }
    let sweetAlert = SweetAlert()
    @objc func handleNetResponse(data:Data?) {
        if let dat = data{
            do{
                let mappedValue = try jsonDecoder.decode(QRResponse.self, from: dat)
                if mappedValue.success!
                {
                    checkIfOverwriteAPI()
                    //                    self.navigationController?.popViewController(animated: true)
                    return
                    
                }else{
                    //                    self.navigationController?.popViewController(animated: true)
                    
                    SweetAlert().showAlert("Invalid QR Code!", subTitle: "Please try again later!", style:.error, buttonTitle: "OK") { (Void) in
                        self.navigationController?.popViewController(animated: true)
                    }
                    
                    //                    self.snackbarView.showSnackBar(view: self.view, bgColor: UIColor.red, text: mappedValue.message!, textColor: UIColor.white, interval: Int(2.0))
                    //                    session.startRunning()
                    return
                }
            }catch let err {
                print(err)
            }
        }
    }
    //   Access Code---- ADHpiRV
    /// Set attendance API Call
    func checkIfOverwriteAPI()
    {
        let mobile_number = UserDefaults.standard.value(forKey: "mobile_num") as! String
        let request = CheckOverWriteQRRequest(mobile_number: mobile_number, type: type)
        self.postDataFromNet(url: Urls.checkIfOverWrite.rawValue , requestData: request, selector: #selector(self.handlecheckIfOverwriteResponse(data:)))
    }
    var msg : String?
    @objc func handlecheckIfOverwriteResponse(data:Data?) {
        if let dat = data{
            do{
                let mappedValue = try jsonDecoder.decode(QRResponse.self, from: dat)
                if mappedValue.success!
                {
                    showAlertForConfirmation()
//                    setAttenandaceAPICall()
                    return
                    
                }else{
                    msg = mappedValue.message!
                    switch msg{
                    case "Need to Checkin first":
                        SweetAlert().showAlert("Invalid Request!", subTitle: "Please try check-in first!", style:.error, buttonTitle: "OK") { (Void) in
                            self.navigationController?.popViewController(animated: true)
                        }
                        
                    case "You can not Check In again":
                        SweetAlert().showAlert("Invalid Entry !", subTitle: "You already checked out for today.!", style:.error, buttonTitle: "OK") { (Void) in
                            self.navigationController?.popViewController(animated: true)
                        }
                        
                    default:
                        let type = UserDefaults.standard.value(forKey: "type") as! String == "checkin" ? "check-in" : "check-out"
                        
                        SweetAlert().showAlert("Overwrite!", subTitle: "Would you like to overwrite \(type) attendance?", style: .none, buttonTitle:"Cancel", buttonColor:#colorLiteral(red: 0.4745098039, green: 0.4745098039, blue: 0.4745098039, alpha: 1) , otherButtonTitle:  "Ok", otherButtonColor: #colorLiteral(red: 0.7333333333, green: 0.8941176471, blue: 0.9647058824, alpha: 1)) { (isOtherButton) -> Void in
                            if isOtherButton{
                                self.navigationController?.popViewController(animated: true)
                            }
                            else {
                                self.setAttenandaceAPICall()
                                
                            }
                        }
                        
                    }
                }
            }catch let err {
                print(err)
            }
        }
    }
    
    func showAlertForConfirmation(){
        SweetAlert().showAlert("Attendance!", subTitle: "Click OK to submit your entry. \n CANCEL to erase time data.", style: .none, buttonTitle:"Cancel", buttonColor:#colorLiteral(red: 0.4745098039, green: 0.4745098039, blue: 0.4745098039, alpha: 1) , otherButtonTitle:  "Ok", otherButtonColor: #colorLiteral(red: 0.7333333333, green: 0.8941176471, blue: 0.9647058824, alpha: 1)) { (isOtherButton) -> Void in
            if isOtherButton{
                self.navigationController?.popViewController(animated: true)
                
            }
            else {
                self.setAttenandaceAPICall()
                }
            }
    }
    
    
    /// Set attendance API Call
    func setAttenandaceAPICall()
    {
        let user_id = UserDefaults.standard.value(forKey: "user_id")
        let request = SetAttendanceRequest(user_id: "\(user_id!)", access_point_id: access_point_id, type: type)
        self.postDataFromNet(url: Urls.setAttendance.rawValue , requestData: request, selector: #selector(self.handleSetAttendanceResponse(data:)))
    }
    
    @objc func handleSetAttendanceResponse(data:Data?) {
        if let dat = data{
            do{
                let mappedValue = try jsonDecoder.decode(QRResponse.self, from: dat)
                if mappedValue.success!{
                    switch mappedValue.message{
                case  "Welcome" :
//                    SweetAlert().showAlert("Attendance!", subTitle: "Click OK to submit your entry. \n CANCEL to erase time data.", style: .none, buttonTitle:"Cancel", buttonColor:#colorLiteral(red: 0.4745098039, green: 0.4745098039, blue: 0.4745098039, alpha: 1) , otherButtonTitle:  "Ok", otherButtonColor: #colorLiteral(red: 0.7333333333, green: 0.8941176471, blue: 0.9647058824, alpha: 1)) { (isOtherButton) -> Void in
//                        if isOtherButton{
//                            self.navigationController?.popViewController(animated: true)
//
//                        }
//                        else {
//
                            SweetAlert().showAlert("Checked-In Successfully!", subTitle: "Thanks!", style:.success, buttonTitle: "OK") { (Void) in
                                self.navigationController?.popViewController(animated: true)
                            }
//                        }
//                    }
                    case "Good Bye":
                        SweetAlert().showAlert("Checked-out Successfully!!", subTitle: "Thanks!", style:.success, buttonTitle: "OK") { (Void) in
                            self.navigationController?.popViewController(animated: true)
                        }
                    default : break
                    return
                    
                }
                }
                else{
                    msg = mappedValue.message!
                    switch msg{
                    case "Admin needs to Setup the Attendance Setting" :
                        SweetAlert().showAlert("Wrong attendance Settings!..", subTitle: "Please contact administrator!", style:.error, buttonTitle: "OK") { (Void) in
                        self.navigationController?.popViewController(animated: true)
                        }
                    case "Invalid Access Point":
                        SweetAlert().showAlert("Invalid Access Point!..", subTitle: "Please try valid access point!", style:.error, buttonTitle: "OK") { (Void) in
                            self.navigationController?.popViewController(animated: true)
                        }
                    default:
                        SweetAlert().showAlert("Invalid Request!..", subTitle: "Please try Check-in first!", style:.error, buttonTitle: "OK") { (Void) in
                            self.navigationController?.popViewController(animated: true)
                        }
                        
                    }
                    
//                    SweetAlert().showAlert("Invalid Request!", subTitle: msg, style:.error, buttonTitle: "OK") { (Void) in
//                        self.navigationController?.popViewController(animated: true)
//                    }
                    //                    self.performSegue(withIdentifier: "popUpSegue", sender: "QRScan")
                    
                    //                    self.snackbarView.showSnackBar(view: self.view, bgColor: UIColor.red, text: mappedValue.message!, textColor: UIColor.white, interval: Int(2.0))
                    return
                }
            }catch let err {
                print(err)
            }
        }
    }
    
    @IBAction func bckBtnTapped(_ sender : UIButton)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let identifer = segue.identifier
        switch  identifer {
        case "popUpSegue":
            let dest = segue.destination as! PopUpVC
            dest.fromVC = sender as? String
            dest.message = msg
            return
        default:
            break
        }
    }
}



