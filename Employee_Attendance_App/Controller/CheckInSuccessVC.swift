//
//  CheckInSuccessVC.swift
//  Employee_Attendance_App
//
//  Created by Renendra Saxena on 29/10/18.
//  Copyright © 2018 Developers Group. All rights reserved.
//

import UIKit

class CheckInSuccessVC: UIViewController {
    
    //    MARK: Initial methods

    override func viewDidLoad() {
        super.viewDidLoad()

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //    MARK: Buttons Action
    @IBAction func crossBtnTappeed(_ sender : UIButton){
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func oKBtnTappeed(_ sender : UIButton){
        self.dismiss(animated: true, completion: nil)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
