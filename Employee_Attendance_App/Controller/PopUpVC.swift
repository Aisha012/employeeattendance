//
//  PopUpVC.swift
//  Employee_Attendance_App
//
//  Created by Renendra Saxena on 30/10/18.
//  Copyright © 2018 Developers Group. All rights reserved.
//

import UIKit
import MBProgressHUD

class PopUpVC: UIViewController , UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet var tableView : UITableView!
    //    @IBOutlet var deviceIdLbl : UILabel!
    var fromVC : String?
    var message :String?
    override func viewDidLoad() {
        super.viewDidLoad()
//        tableView.estimatedRowHeight = 120.0
        
        //        deviceIdLbl.text = UserDefaults.standard.value(forKey: "device_id") as! String
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func cancelBtnTapped(_ sender : UIButton){
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func OKBackBtnTapped(_ sender : UIButton){
        self.navigationController?.popViewController(animated: true)
        
    }
    @IBAction func copyBtnTapped(_ sender : UIButton){
        UIPasteboard.general.string = UserDefaults.standard.value(forKey: "device_id") as! String
        let hud = MBProgressHUD.showAdded(to: self.view, animated: true)
        
        hud.labelText = "Copied"
        hud.hide(animated: true, afterDelay: 2.0)
    }
    
    //    MARK: table view Delegate and dataSource methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if fromVC == "notValidContact" {
            return 150
        }else if (fromVC == "QRScan"){
            return 200
        }
        return 230
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : UITableViewCell!
        if fromVC == "QRScan"{
            cell = tableView.dequeueReusableCell(withIdentifier: "Cell2", for: indexPath)
            let lblTxt = cell.viewWithTag(1) as! UILabel
            lblTxt.text = message
            return cell
        }else if fromVC == "notValidContact"{
            cell = tableView.dequeueReusableCell(withIdentifier: "Cell3", for: indexPath)
            let lblTxt = cell.viewWithTag(1) as! UILabel
            lblTxt.text = message
            return cell
            
        }
        else{
            cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
            let lblTxt = cell.viewWithTag(1) as! UILabel
            let deviceLblText = cell.viewWithTag(2) as! UILabel
            lblTxt.text = message
//            lblTxt.text = (fromVC == "blankMobile") ? "Please enter the number to use the app!" : (fromVC == "notValidContact") ? "Enter a valid Mobile Number." : "Contact your manager & get the device registered."
            deviceLblText.text = (fromVC == "notRegisterMobile") ? UserDefaults.standard.value(forKey: "device_id") as! String : ""
            return cell
        }
    }
    override func viewDidLayoutSubviews(){
        tableView.frame = CGRect(x: tableView.frame.origin.x, y:  tableView.frame.origin.y, width: tableView.frame.size.width, height: tableView.contentSize.height)
        tableView.reloadData()
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
