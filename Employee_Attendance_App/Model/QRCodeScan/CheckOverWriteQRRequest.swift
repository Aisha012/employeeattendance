//
//  CheckOverWriteQRRequest.swift
//  Employee_Attendance_App
//
//  Created by Renendra Saxena on 31/10/18.
//  Copyright © 2018 Developers Group. All rights reserved.
//

import Foundation
struct CheckOverWriteQRRequest: Codable {
    var mobile_number: String!
    var type: String!
}
