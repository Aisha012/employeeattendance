//
//  QRRequest.swift
//  Employee_Attendance_App
//
//  Created by Renendra Saxena on 30/10/18.
//  Copyright © 2018 Developers Group. All rights reserved.
//

import Foundation
struct QRRequest: Codable {
    var current_time: String!
    var qrcode_time: String!
    var mobile_number : String!
    
}
