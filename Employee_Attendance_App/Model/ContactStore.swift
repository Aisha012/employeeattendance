//
//  ContactStore.swift
//  Employee_Attendance_App
//
//  Created by Renendra Saxena on 29/10/18.
//  Copyright © 2018 Developers Group. All rights reserved.
//

import Foundation
struct ContactStore {
    let givenName : String?
    let familyName  :String?
    let number : String?
}
