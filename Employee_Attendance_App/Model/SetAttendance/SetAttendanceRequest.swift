//
//  SetAttendanceRequest.swift
//  Employee_Attendance_App
//
//  Created by Renendra Saxena on 31/10/18.
//  Copyright © 2018 Developers Group. All rights reserved.
//

import Foundation
struct SetAttendanceRequest: Codable {
    var user_id: String?
    var access_point_id: String?
    var type : String?
}
