//
//  LoginResponse.swift
//  Employee_Attendance_App
//
//  Created by Renendra Saxena on 30/10/18.
//  Copyright © 2018 Developers Group. All rights reserved.
//

import Foundation
struct LoginResponse : Codable {
    let success : Bool?
    let data : LoginResponseData?
    let message : String?
    
    enum CodingKeys: String, CodingKey {
        
        case success = "success"
        case data = "data"
        case message = "message"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        success = try values.decodeIfPresent(Bool.self, forKey: .success)
        data = try values.decodeIfPresent(LoginResponseData.self, forKey: .data)
        message = try values.decodeIfPresent(String.self, forKey: .message)
    }
}
struct QRResponse : Codable {
    let success : Bool?
//    let data : NSMutableArray?
    let message : String?
    
    enum CodingKeys: String, CodingKey {
        
        case success = "success"
//        case data = "data"
        case message = "message"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        success = try values.decodeIfPresent(Bool.self, forKey: .success)
//        data = try values.decodeIfPresent(NSMutableArray.self, forKey: .data)
        message = try values.decodeIfPresent(String.self, forKey: .message)
    }
    
}
