//
//  LoginResponseData.swift
//  Employee_Attendance_App
//
//  Created by Renendra Saxena on 30/10/18.
//  Copyright © 2018 Developers Group. All rights reserved.
//

import Foundation
struct LoginResponseData : Codable {
    let id : Int?
    let name : String?
    let email : String?
    let status : String?
    let device_id : String?
    
    enum CodingKeys: String, CodingKey {
        
        case id = "id"
        case name = "name"
        case email = "email"
        case status = "status"
        case device_id = "device_id"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
        name = try values.decodeIfPresent(String.self, forKey: .name)
        email = try values.decodeIfPresent(String.self, forKey: .email)
        status = try values.decodeIfPresent(String.self, forKey: .status)
        device_id = try values.decodeIfPresent(String.self, forKey: .device_id)
    }
    
}
