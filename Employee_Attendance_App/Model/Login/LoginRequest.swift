//
//  LoginRequest.swift
//  Employee_Attendance_App
//
//  Created by Renendra Saxena on 30/10/18.
//  Copyright © 2018 Developers Group. All rights reserved.
//

import Foundation
struct LoginRequest: Codable {
    var mobile_number: String?
    var device_id: String?
    var model_name :String?
    var user_name : String?
    var version_code: String?
    var model : String?
}
